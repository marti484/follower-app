#include "loginterface.h"
#include <QDebug>

loginterface::loginterface()
{
    lastCheckedIndex=0; // SUKI - name typo
}

int loginterface::sizeOfLog()
{
    int size;

    size=theLog.getSize(); // SUKI - correct API call

    return size;

}

Entry * loginterface::pullLatest()
{
    qDebug() <<"inside pullLatest<<loginterface";


    Entry * anEntry=theLog.getLatest();
    lastCheckedIndex=theLog.getSize()-1;
    theLog.pullGlucoseData();


    return anEntry;



}

Entry * loginterface::pullFromLastChecked()
{
    qDebug() <<"inside pullfromLastChecked<<loginterface";

    qDebug() <<"sizeofLog:"<<theLog.getSize();
    if(theLog.getSize() >= 0) // SUKI - correct API call
    {

        Entry * object1=theLog.getEntry(lastCheckedIndex);
        lastCheckedIndex++;

        qDebug() <<"returning"<<object1->bloodGlucose;
        return object1;

    }
    //else SUKI - this wasnt declared

    else
        return NULL;
}



void loginterface::logPullFromProxy()
{
    theLog.pullGlucoseData();
//calls  function in log to get last values from proxy.
//Populates the log list.

    return;

}

void loginterface::resetLastCheckedIndex()
{
    lastCheckedIndex=0;
}
