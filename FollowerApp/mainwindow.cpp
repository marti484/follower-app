
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "notificationcontroller.h"
#include "updateinterface.h"
#include "QHBoxLayout"
#include <QTimer>
#include <QtCore>
#include <QDate>
#include <QTime>
#include <QMediaPlayer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    controller.updateLog();
    ui->setupUi(this);

    //QObject::connect(ui->actionSTOPALL,SIGNAL(triggered(bool)),this,SLOT(on_actionSTOPALL_clicked()));


    intervalTimer=new QTimer(this);
    bgEvaltimer=new QTimer(this);

    QObject::connect(bgEvaltimer,SIGNAL(timeout()),this,SLOT(startEvaluating()));
    QObject::connect(intervalTimer, SIGNAL(timeout()),this,SLOT(pullNewest()));

    ui->tableWidget->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Stretch);
    ui->StatusText->setText("STATUS");
    ui->StatusText->setStyleSheet("font color='#606060'");

    Green.setColor(QPalette::WindowText,QColor(76,153,0));
    Yellow.setColor(QPalette::WindowText,QColor(204,204,0));
    Orange.setColor(QPalette::WindowText,QColor(255,128,0));
    Red.setColor(QPalette::WindowText,Qt::red);



}

MainWindow::~MainWindow()
{
    delete ui;
}


//_____________________________________________________________
//SPINBOX AND SLIDER SIGNALS AND SLOTS
void MainWindow::on_spinBox_valueChanged(int arg1)
{
    ui->horizontalSlider->setValue((arg1));
    ui->horizontalSlider->show();
}

void MainWindow::on_horizontalSlider_valueChanged(int value)
{
    ui->spinBox->setValue(value);
    ui->spinBox->show();
}
//____________________________________________________________





void MainWindow::on_updateNowButton_clicked()
{
    Entry * anEntry= controller.timeToUpdate();
    updateTheTable(anEntry);

    return ;
}


//_________________________________________________________
void MainWindow::on_intervalButton_clicked()
{
    int interval;
    interval=ui->horizontalSlider->value();
    ui->intervalButton->setText("Tracking User");
    ui->intervalButton->setChecked(true);
    qDebug() <<interval;
    startTimer(interval);
    return;

}

//____________________________________________________
void MainWindow::pullNewest()
{
    qDebug() <<"inside pullNewest<<mainWindow";
    Entry * anEntry=controller.timeToUpdate();
    controller.updateLog();
    updateTheTable(anEntry);
    return;

}

//__________________________________________________
void MainWindow::startEvaluating()
{
    qDebug() <<"inside StartEvaluating<<mainWindow";
    Entry* anEntry=controller.beginAnEvaluation();
    qDebug() <<"pulled anEntry inside startEvaluating";

    if(anEntry->alert ==true)
    {
        updateTheTable(anEntry);
        controller.updateLog();

        return;
    }
    else if(anEntry->alert ==false)
    {
        controller.updateLog();

        return;
    }
}

//_____________________________________________________
void MainWindow::startTimer(int intervalFactor)
{
    qDebug() <<"inside startTimer<<mainwindow.cpp";

    //QTimer *intervalTimer=new QTimer(this);
    //QTimer *bgEvaltimer=new QTimer(this);
    //made the timers as attributes in the class
    //instead and then assigned them here
    //intervalTimer=new QTimer(this);
    //bgEvaltimer=new QTimer(this);

    //QObject::connect(bgEvaltimer,SIGNAL(timeout()),this,SLOT(startEvaluating()));
    //QObject::connect(intervalTimer, SIGNAL(timeout()),this,SLOT(pullNewest()));

    /*if(bgEvaltimer->isActive()==false)
    {
        QObject::connect(bgEvaltimer,SIGNAL(timeout()),this,SLOT(startEvaluating()));
        bgEvaltimer->start(1000);
    }*/


    if(intervalTimer->isActive() == true)
        intervalTimer->stop();
    if(bgEvaltimer->isActive() == true)
        bgEvaltimer->stop();

    bgEvaltimer->start(1000);
    intervalTimer->start((intervalFactor * 1000));


}

//__________________________________________________
void MainWindow::updateTheTable(Entry* anEntry)
{
    /*
    QDate theDate= QDate::currentDate();
    QString date=theDate.toString("MM/dd/yyyy");
    QTime theTime= QTime::currentTime();
    QString time=theTime.toString("hh:mm:ss");

    */
    QTableWidgetItem *messageItem=new QTableWidgetItem(anEntry->message);
    QTableWidgetItem *dateItem=new QTableWidgetItem(anEntry->date /*date*/);
    QTableWidgetItem *timeItem=new QTableWidgetItem(anEntry->time/*time*/);


    QTableWidgetItem *bloodGlucose=new QTableWidgetItem(QString::number(anEntry->bloodGlucose));

    messageItem->setForeground(Qt::black);
    dateItem->setForeground(Qt::black); // SUKI - typo
    timeItem->setForeground(Qt::black);
    bloodGlucose->setForeground(Qt::black);

    ui->tableWidget->insertRow(0);


    if(anEntry->displayColor==4)
    {

        messageItem->setBackground(Qt::red);
        dateItem->setBackground(Qt::red);
        timeItem->setBackground(Qt::red);
        bloodGlucose->setBackground(Qt::red);
        ui->tableWidget->setItem(0,0,dateItem);
        ui->tableWidget->setItem(0,1,timeItem);
        ui->tableWidget->setItem(0,2,messageItem);
        ui->tableWidget->setItem(0,3,bloodGlucose);
        ui->StatusText->setText("ALERT");
        ui->StatusText->setPalette(Red);
        //QMediaPlayer *music = new QMediaPlayer();
        //music->setMedia(QUrl("qrc:/sounds/alarm_sound.mp3"));
        //music->play();
    }
    else if(anEntry->displayColor==3)
    {
        messageItem->setBackground(QColor(255,128,0)); // SUKI - typo
        dateItem->setBackground(QColor(255,128,0));// SUKI - typo
        timeItem->setBackground(QColor(255,128,0));// SUKI - typo
        bloodGlucose->setBackground(QColor(255,128,0));// SUKI - typo

        ui->tableWidget->setItem(0,0,dateItem);
        ui->tableWidget->setItem(0,1,timeItem);
        ui->tableWidget->setItem(0,2,messageItem);
        ui->tableWidget->setItem(0,3,bloodGlucose);

        ui->StatusText->setText("WARNING");
        ui->StatusText->setPalette(Orange);
    }
    else if(anEntry->displayColor==2)
    {
        messageItem->setBackground(QColor(255,255,0));
        dateItem->setBackground(QColor(255,255,0));
        timeItem->setBackground(QColor(255,255,0));
        bloodGlucose->setBackground(QColor(255,255,0));


        ui->tableWidget->setItem(0,0,dateItem);
        ui->tableWidget->setItem(0,1,timeItem);
        ui->tableWidget->setItem(0,2,messageItem);
        ui->tableWidget->setItem(0,3,bloodGlucose);

        ui->StatusText->setText("CAUTION");
        ui->StatusText->setPalette(Yellow);

    }
    else if(anEntry->displayColor==1)
    {
        messageItem->setBackground(Qt::green);
        dateItem->setBackground(Qt::green);
        timeItem->setBackground(Qt::green);
        bloodGlucose->setBackground(Qt::green);


        ui->tableWidget->setItem(0,0,dateItem);
        ui->tableWidget->setItem(0,1,timeItem);
        ui->tableWidget->setItem(0,2,messageItem);
        ui->tableWidget->setItem(0,3,bloodGlucose);
        ui->StatusText->setText("GOOD");
        ui->StatusText->setPalette(Green);

    }

    ui->tableWidget->resizeColumnToContents(0);
    ui->tableWidget->resizeColumnToContents(1);
   // ui->tableWidget->resizeColumnToContents(2);
    ui->tableWidget->resizeColumnToContents(3);
    //ui->tableWidget->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Stretch);
    qDebug() <<"__________________________";
    return;
}


//___________________________________________

void MainWindow::on_stopButton_clicked()
{
    intervalTimer->stop();
    ui->intervalButton->setChecked(false);
    ui->intervalButton->setText("Set Sync \n Frequency");

}
/*
void MainWindow::on_actionSTOPALL_clicked()
{
    intervalTimer->stop();
    bgEvaltimer->stop();

    ui->intervalButton->setChecked(false);
    ui->intervalButton->setText("Set Sync \n Frequency");

}
*/

void MainWindow::on_actionSTOPALL_triggered()
{
    intervalTimer->stop();
    bgEvaltimer->stop();

    ui->intervalButton->setChecked(false);
    ui->intervalButton->setText("Set Sync \n Frequency");

}
