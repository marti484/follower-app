#-------------------------------------------------
#
# Project created by QtCreator 2015-10-08T16:49:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets multimedia

TARGET = FollowerApp
TEMPLATE = app


SOURCES += \
    messevaluator.cpp \
    log.cpp \
    proxy.cpp \
    notificationcontroller.cpp \
    updateinterface.cpp \
    main.cpp \
    loginterface.cpp \
    mainwindow.cpp \

HEADERS  += \
    mainwindow.h \
    messevaluator.h \
    log.h \
    notificationcontroller.h \
    proxy.h \
    updateinterface.h \
    loginterface.h \


FORMS    += \
    mainwindow.ui \

RESOURCES += \
    msc.qrc
