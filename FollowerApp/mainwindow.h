#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "log.h"
#include "notificationcontroller.h"
#include <QTimer>
#include <QtCore>
#include <QPalette>


namespace Ui {
class MainWindow;
}


/*_____________________________________________

  */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    //void startEvaluating();
    void startTimer(int);
   // void pullNewest();
    void updateTheTable(Entry*);


private slots:

    //void on_listWidget_activated(const QModelIndex &index);

    void on_spinBox_valueChanged(int arg1);

    void on_horizontalSlider_valueChanged(int value);

    void on_intervalButton_clicked();

    void on_updateNowButton_clicked();
    void on_stopButton_clicked();

    void on_actionSTOPALL_triggered();

public slots:
    void startEvaluating();
    void pullNewest();
    //void on_actionSTOPALL_clicked();
private:
    Ui::MainWindow *ui;
    notification controller;
    QTimer *intervalTimer;
    QTimer *bgEvaltimer;
    QPalette Green;
    QPalette Orange;
    QPalette Yellow;
    QPalette Red;
};

//_________________________________________________

#endif // MAINWINDOW_H
