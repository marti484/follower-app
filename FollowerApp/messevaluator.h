#pragma once
#include "loginterface.h"
#include "log.h"



/*Purpose: This class will fetch the single data value from the log file and generate a composed message
 * The composed message will have the time, blood/glucose value, and a message dependent on the danger of
 * the data.
 *
 * - this class methods wont be called unless called on by the notification controller.
 * -controller has the settings fro when to get the notifications.
 *
 */

class MessgGenerator
{

private:

    QString goodMessage;
    QString cautionMessage;
    QString warningMessage;
    QString dangerMessage;
    int dangerColorTag;//red 4
    int warningColorTag;//orange 3
    int cautionColorTag;//yellow 2
    int goodColorTag;//green 1
    int maxNormal; //values to set for our evaluation of whats dangerous or not
    int minNormal; //minimun blood glucose can be before is dangerous.
    loginterface connectionToLog;

public:

    MessgGenerator();
    ~MessgGenerator();
    void dangEvaluator(int,Entry *);//evaluates dangValue based on data from log file
    //void sendMessage();//calls fetch method from notification controller to send data.
    void messagComposer(Entry *); //
    Entry * pullLatestData(); //get newest entry from log file
    Entry * processEntries();
    void makeLogPull();
};

 // MESSEVALUATOR







