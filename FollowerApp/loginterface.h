#pragma once
#include "log.h"


class loginterface
{

public:
    loginterface();
    Entry * pullLatest();
    Entry * pullFromLastChecked();
    void logPullFromProxy();
    int sizeOfLog();
    void resetLastCheckedIndex();



private:
    Log theLog;
    int lastCheckedIndex;


};
