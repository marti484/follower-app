#include "messevaluator.h"
#include "log.h"
#include <QDebug>

/*
 * Last update:11/03/2015
 *
 * Description:
 * function is in charge of taking the max danger limit and the minimum danger limit
 * and calculate a value between 1-10 based on where the blood glucose level stands
 * between, below, or above the set minimum level.
 *
 */


MessgGenerator::MessgGenerator()
{
    maxNormal=325;
    minNormal=225;
    goodMessage="Levels are within normal range";
    cautionMessage="Attention: Levels outside normal range";
    warningMessage="Warning: Approaching dangerous level";
    dangerMessage="ALERT! BLOOD GLUCOSE DANGEROUS";
    dangerColorTag=4;
    warningColorTag=3;
    cautionColorTag=2;
    goodColorTag=1;

}
MessgGenerator::~MessgGenerator()
{





}

void MessgGenerator::dangEvaluator(int glucose,Entry * anEntry)
{
    if(glucose==maxNormal||glucose==minNormal)
    {
        anEntry->dangLevel=0;
        anEntry->displayColor=goodColorTag;//green color

        return;
    }
    if( glucose > maxNormal || glucose <minNormal)
    {
        //type of evaluation
        if(glucose>=(maxNormal+100) || glucose<=(minNormal-100)){
                anEntry->dangLevel=10;
                anEntry->alert=true;
                anEntry->displayColor=dangerColorTag;
                return;
        }
        else if(glucose>=(maxNormal+90) || glucose<=(minNormal-90)){
                anEntry->dangLevel=9;
                anEntry->alert=true;
                anEntry->displayColor=dangerColorTag;

                return;
        }
        else if(glucose>=(maxNormal+80) || glucose<=(minNormal-80)){
                anEntry->dangLevel=8;
                anEntry->alert=true;
                anEntry->displayColor=dangerColorTag;
                return;
        }
        else if(glucose>(maxNormal+70) || glucose<=(minNormal-70)){
                anEntry->dangLevel=7;
                anEntry->alert=true;
                anEntry->displayColor=dangerColorTag;
                return;
        }
        else if(glucose>=(maxNormal+60) || glucose<=(minNormal-60)){
                anEntry->dangLevel=6;
                anEntry->displayColor=warningColorTag;
                return;
        }
        else if(glucose>=(maxNormal+50) || glucose<=(minNormal-50)){
                anEntry->dangLevel=5;
                anEntry->displayColor=warningColorTag;
                return;
        }
        else if(glucose>=(maxNormal+40) || glucose<=(minNormal-40)){
                anEntry->dangLevel=4;
                anEntry->displayColor=warningColorTag;
                return;
        }
        else if(glucose>=(maxNormal+30) || glucose<=(minNormal-30)){
                anEntry->dangLevel=3;
                anEntry->displayColor=cautionColorTag;
                return;
        }
        else if(glucose>=(maxNormal+20) || glucose<=(minNormal-20)){
                anEntry->dangLevel=2;
                anEntry->displayColor=cautionColorTag;
                return;
        }
        else if(glucose>=(maxNormal+1) || glucose<=(minNormal-1)){
                anEntry->dangLevel=1;
                anEntry->displayColor=cautionColorTag;//yellow
                return;

         }

    }
    else if(glucose<maxNormal && glucose > minNormal)
    {

        anEntry->dangLevel=0;
        anEntry->displayColor=goodColorTag;//green color

        return;

    }

}



void MessgGenerator::messagComposer(Entry* anEntry) //
{


    if (anEntry->displayColor ==4)
    {
        anEntry->message=dangerMessage;
        return;

    }
    else if(anEntry->displayColor==3)
    {
        anEntry->message=warningMessage;
        return;

    }
    else if(anEntry->displayColor==2)
    {
        anEntry->message=cautionMessage;
        return;
    }
    else if(anEntry->displayColor==1)
    {
        anEntry->message=goodMessage;
        return;
    }

}
Entry* MessgGenerator::pullLatestData() //get data from log file
{
    qDebug() <<"inside pullLatestData<<mssgGen";

       Entry * anEntry= connectionToLog.pullLatest();

       if(anEntry->bloodGlucose == -1)//outof log item-Cris
       {
           connectionToLog.resetLastCheckedIndex();
           anEntry=connectionToLog.pullLatest();
       }

       int glucoseVal= anEntry->bloodGlucose;
       dangEvaluator(glucoseVal,anEntry);
       messagComposer(anEntry);
       makeLogPull();
       return anEntry;

}

Entry* MessgGenerator::processEntries()
{

    qDebug() <<"inside processEntries<messgGenerator";

    Entry * anEntry= connectionToLog.pullFromLastChecked();
    if(anEntry->bloodGlucose == -1)//outof log item-Cris
    {
        connectionToLog.resetLastCheckedIndex();
        anEntry=connectionToLog.pullLatest();
    }



    int glucoseVal=anEntry->bloodGlucose;
    dangEvaluator(glucoseVal,anEntry);
    messagComposer(anEntry);
    makeLogPull();

    return anEntry;


}

void MessgGenerator::makeLogPull()
{
    connectionToLog.logPullFromProxy();
    return;
}


