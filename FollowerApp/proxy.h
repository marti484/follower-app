#pragma once
#include <iostream>
#include <QString>
#include <QObject>
#include <vector>



class Proxy
{
protected:

    //int m_dataValues[size];
    std::vector <int> m_dataValues;
    int m_lastValueIndex;

public:

    Proxy();
    int sendValue();


};

