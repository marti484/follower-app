#ifndef INTIALPAGE_H
#define INTIALPAGE_H

#include <QWidget>

namespace Ui {
class InitialPage;
}



class IntialPage : public QWidget
{
    Q_OBJECT

public:
    explicit IntialPage(QWidget *parent = 0);
    ~IntialPage();

private slots:
    void on_beginButton_clicked();

private:
    Ui::InitialPage *ui;
};

#endif // INTIALPAGE_H
