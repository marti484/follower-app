#include "mainwindow.h"
#include <QApplication>
#include "initialpage.h"
#include "log.h"
#include "notificationcontroller.h"



int main(int argc, char *argv[])
{
    QApplication prog(argc, argv);
    /*
    InitialPage frontPage;
    frontPage.setWindowTitle("Front Page");
    frontPage.show();
    */
    MainWindow startWindow;
    startWindow.setWindowTitle("FollowerApp");
    startWindow.show();

    return prog.exec();

}
