#pragma once
#include <QList>
#include <QDateTime>
#include <QString>
#include "proxy.h"
//#include "loginterface.h" // SUKI - Circular reference
#include <iostream>




struct Entry;




class Log
{

protected:

    QList<Entry*> logList; // SUKI - made this a Entry ptr

    int lastValuePulled;
    Proxy server;//integer to keep tract at for
    //what part of the proxy data has been accessed

public:

    Log();//constructor
    ~Log();

    void storeData(int);
    //stores the data value into an Entry object and stores it.
    //  const Entry *getEntry(string, int); //public to to retrieve most recent value.

    Entry * getLatest();//returns latest
    Entry * getEntry(int);

    void pullGlucoseData(); //retrieves data from proxy.

    int getSize() {return logList.size();} // SUKI - Added this API

};


struct Entry
{


    QString date;
    QString time;
    int  bloodGlucose;
    int dangLevel;//place to store danger value as this object gets passed over to the messageeval
    QString message;
    int displayColor;//1=green 2=yellow 3=orange
    //4=red
    bool alert;


    Entry()
    {
        date="\0";
        time="\0";
        bloodGlucose=0;
        dangLevel=0;
        alert=false;
    }

};







