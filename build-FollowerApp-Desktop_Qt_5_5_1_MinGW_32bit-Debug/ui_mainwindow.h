/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionSTOPALL;
    QWidget *centralWidget;
    QPushButton *updateNowButton;
    QPushButton *intervalButton;
    QPushButton *stopButton;
    QLabel *StatusText;
    QWidget *layoutWidget;
    QFormLayout *formLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QSlider *horizontalSlider;
    QHBoxLayout *horizontalLayout;
    QSpinBox *spinBox;
    QLabel *label_3;
    QTableWidget *tableWidget;
    QLabel *label;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QMenuBar *menuBar;
    QMenu *menuFollower;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(917, 549);
        QPalette palette;
        QBrush brush(QColor(240, 240, 240, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        QBrush brush1(QColor(227, 227, 227, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush1);
        QBrush brush2(QColor(255, 255, 255, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush2);
        QBrush brush3(QColor(208, 240, 236, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Window, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush3);
        MainWindow->setPalette(palette);
        actionSTOPALL = new QAction(MainWindow);
        actionSTOPALL->setObjectName(QStringLiteral("actionSTOPALL"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        updateNowButton = new QPushButton(centralWidget);
        updateNowButton->setObjectName(QStringLiteral("updateNowButton"));
        updateNowButton->setGeometry(QRect(690, 100, 131, 41));
        QFont font;
        font.setPointSize(11);
        updateNowButton->setFont(font);
        updateNowButton->setCursor(QCursor(Qt::PointingHandCursor));
        intervalButton = new QPushButton(centralWidget);
        intervalButton->setObjectName(QStringLiteral("intervalButton"));
        intervalButton->setGeometry(QRect(680, 290, 141, 71));
        QFont font1;
        font1.setPointSize(10);
        intervalButton->setFont(font1);
        intervalButton->setCursor(QCursor(Qt::PointingHandCursor));
        intervalButton->setCheckable(true);
        intervalButton->setAutoDefault(false);
        intervalButton->setFlat(false);
        stopButton = new QPushButton(centralWidget);
        stopButton->setObjectName(QStringLiteral("stopButton"));
        stopButton->setGeometry(QRect(690, 390, 121, 61));
        stopButton->setFont(font1);
        stopButton->setCursor(QCursor(Qt::PointingHandCursor));
        StatusText = new QLabel(centralWidget);
        StatusText->setObjectName(QStringLiteral("StatusText"));
        StatusText->setGeometry(QRect(680, 30, 141, 51));
        QPalette palette1;
        QBrush brush4(QColor(176, 96, 255, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush4);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush4);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        StatusText->setPalette(palette1);
        QFont font2;
        font2.setPointSize(14);
        font2.setBold(true);
        font2.setWeight(75);
        StatusText->setFont(font2);
        StatusText->setCursor(QCursor(Qt::BlankCursor));
        StatusText->setFrameShape(QFrame::Panel);
        StatusText->setFrameShadow(QFrame::Raised);
        StatusText->setLineWidth(7);
        StatusText->setAlignment(Qt::AlignCenter);
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(660, 170, 212, 101));
        formLayout = new QFormLayout(layoutWidget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        QFont font3;
        font3.setPointSize(12);
        font3.setItalic(true);
        label_2->setFont(font3);

        verticalLayout_2->addWidget(label_2);

        horizontalSlider = new QSlider(layoutWidget);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        QFont font4;
        font4.setPointSize(8);
        horizontalSlider->setFont(font4);
        horizontalSlider->setCursor(QCursor(Qt::SizeHorCursor));
        horizontalSlider->setAutoFillBackground(false);
        horizontalSlider->setMinimum(1);
        horizontalSlider->setMaximum(15);
        horizontalSlider->setPageStep(15);
        horizontalSlider->setValue(7);
        horizontalSlider->setSliderPosition(7);
        horizontalSlider->setOrientation(Qt::Horizontal);
        horizontalSlider->setInvertedAppearance(false);
        horizontalSlider->setTickPosition(QSlider::TicksBelow);
        horizontalSlider->setTickInterval(1);

        verticalLayout_2->addWidget(horizontalSlider);


        formLayout->setLayout(0, QFormLayout::LabelRole, verticalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        spinBox = new QSpinBox(layoutWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        QFont font5;
        font5.setPointSize(12);
        spinBox->setFont(font5);
        spinBox->setCursor(QCursor(Qt::PointingHandCursor));
        spinBox->setAlignment(Qt::AlignCenter);
        spinBox->setMinimum(1);
        spinBox->setMaximum(15);
        spinBox->setValue(5);

        horizontalLayout->addWidget(spinBox);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        QFont font6;
        font6.setPointSize(12);
        font6.setItalic(true);
        font6.setUnderline(true);
        label_3->setFont(font6);
        label_3->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(label_3);


        formLayout->setLayout(1, QFormLayout::LabelRole, horizontalLayout);

        tableWidget = new QTableWidget(centralWidget);
        if (tableWidget->columnCount() < 4)
            tableWidget->setColumnCount(4);
        QFont font7;
        font7.setPointSize(9);
        font7.setUnderline(false);
        font7.setKerning(true);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        __qtablewidgetitem->setTextAlignment(Qt::AlignCenter);
        __qtablewidgetitem->setFont(font7);
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        __qtablewidgetitem1->setTextAlignment(Qt::AlignCenter);
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        __qtablewidgetitem2->setTextAlignment(Qt::AlignCenter);
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        __qtablewidgetitem3->setTextAlignment(Qt::AlignCenter);
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setGeometry(QRect(1, 65, 591, 411));
        tableWidget->setFrameShape(QFrame::Box);
        tableWidget->setFrameShadow(QFrame::Plain);
        tableWidget->setLineWidth(2);
        tableWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        tableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        tableWidget->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        tableWidget->setTabKeyNavigation(false);
        tableWidget->setSelectionMode(QAbstractItemView::NoSelection);
        tableWidget->setCornerButtonEnabled(false);
        tableWidget->horizontalHeader()->setDefaultSectionSize(118);
        tableWidget->horizontalHeader()->setStretchLastSection(false);
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(120, 10, 321, 37));
        QPalette palette2;
        QBrush brush5(QColor(170, 85, 255, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush5);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush5);
        QBrush brush6(QColor(120, 120, 120, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush6);
        label->setPalette(palette2);
        QFont font8;
        font8.setFamily(QStringLiteral("Segoe UI Semibold"));
        font8.setPointSize(18);
        font8.setBold(true);
        font8.setUnderline(true);
        font8.setWeight(75);
        label->setFont(font8);
        label->setAutoFillBackground(true);
        label->setAlignment(Qt::AlignCenter);
        MainWindow->setCentralWidget(centralWidget);
        tableWidget->raise();
        label->raise();
        layoutWidget->raise();
        updateNowButton->raise();
        intervalButton->raise();
        stopButton->raise();
        StatusText->raise();
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 917, 26));
        menuFollower = new QMenu(menuBar);
        menuFollower->setObjectName(QStringLiteral("menuFollower"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuFollower->menuAction());
        menuFollower->addAction(actionSTOPALL);

        retranslateUi(MainWindow);

        intervalButton->setDefault(false);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Follower Notifications", 0));
        actionSTOPALL->setText(QApplication::translate("MainWindow", "STOP ALL", 0));
        updateNowButton->setText(QApplication::translate("MainWindow", "UpdateNow", 0));
        intervalButton->setText(QApplication::translate("MainWindow", "Set Sync \n"
" Frequency", 0));
        stopButton->setText(QApplication::translate("MainWindow", "Stop Tracking", 0));
        StatusText->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_2->setText(QApplication::translate("MainWindow", "Notification Frequency", 0));
        label_3->setText(QApplication::translate("MainWindow", "seconds", 0));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "Date", 0));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MainWindow", "Time", 0));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("MainWindow", "Message", 0));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("MainWindow", "Blood/Glucose", 0));
        label->setText(QApplication::translate("MainWindow", "Follower Notifications", 0));
        menuFollower->setTitle(QApplication::translate("MainWindow", "Follower", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
