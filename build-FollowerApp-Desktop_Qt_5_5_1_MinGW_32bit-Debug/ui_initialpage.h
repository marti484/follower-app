/********************************************************************************
** Form generated from reading UI file 'initialpage.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INITIALPAGE_H
#define UI_INITIALPAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_IntialPage
{
public:
    QLabel *label;
    QPushButton *beginButton;

    void setupUi(QWidget *IntialPage)
    {
        if (IntialPage->objectName().isEmpty())
            IntialPage->setObjectName(QStringLiteral("IntialPage"));
        IntialPage->resize(766, 487);
        label = new QLabel(IntialPage);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(270, 60, 211, 51));
        QFont font;
        font.setFamily(QStringLiteral("Georgia"));
        font.setPointSize(16);
        font.setBold(true);
        font.setUnderline(false);
        font.setWeight(75);
        font.setStrikeOut(false);
        label->setFont(font);
        label->setScaledContents(true);
        beginButton = new QPushButton(IntialPage);
        beginButton->setObjectName(QStringLiteral("beginButton"));
        beginButton->setGeometry(QRect(260, 210, 201, 71));
        QFont font1;
        font1.setPointSize(14);
        font1.setBold(true);
        font1.setWeight(75);
        beginButton->setFont(font1);

        retranslateUi(IntialPage);

        QMetaObject::connectSlotsByName(IntialPage);
    } // setupUi

    void retranslateUi(QWidget *IntialPage)
    {
        IntialPage->setWindowTitle(QApplication::translate("IntialPage", "Form", 0));
        label->setText(QApplication::translate("IntialPage", "Follower App", 0));
        beginButton->setText(QApplication::translate("IntialPage", "Start Tracking", 0));
    } // retranslateUi

};

namespace Ui {
    class IntialPage: public Ui_IntialPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INITIALPAGE_H
